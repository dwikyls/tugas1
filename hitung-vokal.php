<?php
hitungVokal('wisnu');
hitungVokal('bayu');
hitungVokal('manupraba');

function hitungVokal($word)
{
    $vokals = ['a', 'i', 'u', 'e', 'o'];

    $arr = str_split($word);
    $cekVokal = array_intersect($arr, $vokals);
    $results = array_unique($cekVokal);

    echo "$word = ". count($results) ." yaitu " . implode(", ", $results) . "\n";
}
